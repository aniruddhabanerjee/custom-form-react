import React, { Component } from 'react'
import SignUpForm from './component/SignUpForm'
import Header from './component/Header'

class App extends Component {
  render() {
    return (
      <div className='App'>
        <Header />
        <SignUpForm />
      </div>
    )
  }
}
export default App
