import React from 'react'

export default function FormFooter() {
  return (
    <div className='footer'>
      <h3>Or continue with social profile</h3>
      <div className='footer-login'>
        <i className='fab fa-google'></i>
        <i className='fab fa-twitter'></i>
        <i className='fab fa-facebook-f'></i>
      </div>
      <p>
        Already a member? <span>Login</span>
      </p>
    </div>
  )
}
