import React from 'react'
import Joi from 'joi-browser'

export class Form extends React.Component {
  state = {
    data: {},
    errors: {},
  }

  validate = () => {
    const { error } = Joi.validate(this.state.data, this.schema, {
      abortEarly: false,
    })
    if (!error) return {}

    const errors = {}
    error.details.forEach((item) => {
      errors[item.path[0]] = item.message
    })
    return errors
  }

  validateOnChange = ({ name, value }) => {
    const obj = { [name]: value }
    const schema = { [name]: this.schema[name] }
    const { error } = Joi.validate(obj, schema)

    if (error) {
      return error.details[0].message
    } else {
      return {}
    }
  }

  handleChange = (e) => {
    const errors = { ...this.state.errors }
    const error = this.validateOnChange(e.currentTarget)
    if (Object.keys(error).length > 0) {
      errors[e.currentTarget.name] = error
    } else {
      delete errors[e.currentTarget.name]
    }
    const data = { ...this.state.data }
    data[e.currentTarget.name] = e.currentTarget.value
    this.setState({ data, errors })
  }

  selectHandler = (selectedLanguage) => {
    const data = { ...this.state.data, selectedLanguage }
    this.setState({ data })
  }

  handleSubmit = (e) => {
    e.preventDefault()

    const errors = this.validate()
    this.setState({ errors })
    if (Object.keys(errors).length === 0) {
      this.submitForm()
    } else {
      console.log(errors)
    }
  }
}

export default Form
