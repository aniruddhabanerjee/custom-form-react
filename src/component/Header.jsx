import React, { Component } from 'react'

export class Header extends Component {
  render() {
    return (
      <div className='header-bg'>
        <header className='header'>
          <div className='logo'>
            <i className='fas fa-code'></i> <span>HackTix</span>
          </div>
          <div className='header-info'>
            <h1>Don't have an account?</h1>
            <p>Register to access all the features of our service.</p>
            <p>Learn to code Interactively for free.</p>
            <div className='social-icons'>
              <i className='fab fa-facebook-f'></i>
              <i className='fab fa-twitter'></i>
              <i className='fab fa-google-plus-g'></i>
              <i className='fab fa-instagram'></i>
            </div>
          </div>
        </header>
      </div>
    )
  }
}

export default Header
