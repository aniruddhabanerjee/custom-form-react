import React, { Component } from 'react'

class formGeneretor extends Component {
  state = {
    fieldName: '',
    fieldType: '',
    children: [],
    generate: false,
  }

  addInputFeild = (e) => {
    e.preventDefault()
    this.setState((prevState) => ({
      children: [
        ...prevState.children,
        { name: prevState.fieldName, type: prevState.fieldType },
      ],
    }))
  }

  handleChange = (e) => {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }
  generateForm = () => {
    this.setState({ generate: true })
  }
  render() {
    const children = this.state.children.map((val) => {
      return (
        <div>
          <label>Name of the Label {val.name}</label>
          <br />
          <label>Type of the Field {val.type}</label>
        </div>
      )
    })
    const formElement = this.state.children.map((val) => {
      return (
        <div>
          <label>{val.name}</label>
          <input
            name={val.name}
            type={val.type}
            placeholder={`Enter your ${val.name}`}
          />
        </div>
      )
    })

    return (
      <div className='formGeneretor'>
        {children}
        <form onSubmit={this.addInputFeild}>
          <label>Enter label name</label>
          <input
            name='fieldName'
            value={this.state.fieldName}
            type='text'
            onChange={this.handleChange}
          />
          <label>Enter Type</label>
          <input
            name='fieldType'
            value={this.state.fieldType}
            type='text'
            onChange={this.handleChange}
          />
          <button type='submit'>Add</button>
        </form>
        <button onClick={this.generateForm}>Generate Form</button>
        {this.state.generate ? formElement : ''}
      </div>
    )
  }
}
export default formGeneretor
