import React from 'react'

const Radio = ({ name, onChange, type, error, radioButton }) => {
  return (
    <React.Fragment>
      <div className='radio-group'>
        {radioButton.map((item) => {
          return (
            <div key={item.value} className='radio-btn'>
              <input
                name={name}
                type={type}
                value={item.value}
                onChange={onChange}
              />
              <label>{item.label}</label>
            </div>
          )
        })}
      </div>
      {error ? (
        <small className='error'>{error}</small>
      ) : (
        <small className='error-none'>{''}</small>
      )}
    </React.Fragment>
  )
}

export default Radio

// import React, { Component } from 'react'

// export class Input extends Component {
//   state = {}
//   handleChange = (e) => {
//     const { name, value } = e.target
//     this.setState({ [name]: value })
//     this.props.setInputValue(name.toLowerCase(), value)
//   }
//   render() {
//     const formElement = this.props.inputField.map((val) => {
//       return (
//         <div className='form-group' key={val.name}>
//           <input
//             className='input-box'
//             name={val.name}
//             type={val.type}
//             value={this.state[val.name] || ''}
//             placeholder={`Enter your ${val.name}`}
//             onChange={this.handleChange}
//           />
//         </div>
//       )
//     })
//     return <div>{formElement}</div>
//   }
// }

// export default Input
