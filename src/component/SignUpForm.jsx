import React from 'react'
import Input from './Input'
import Joi from 'joi-browser'
import Select from 'react-select'
import FormFooter from './FormFooter'
import Form from './Form'
import Radio from './Radio'

export class SignUpForm extends Form {
  state = {
    data: {
      name: '',
      password: '',
      username: '',
      email: '',
      phoneNumber: '',
      address: '',
      gender: '',
      selectedLanguage: null,
    },
    errors: {},
    one: false,
  }

  schema = {
    name: Joi.string().required().label('Name'),
    password: Joi.string().required().min(8).max(30).label('Password'),
    username: Joi.string()
      .alphanum()
      .min(8)
      .max(30)
      .required()
      .label('Username'),
    phoneNumber: Joi.number().required().label('Phone Number'),
    email: Joi.string().required().email().label('Email'),
    address: Joi.string().label('Address'),
    selectedLanguage: Joi.object(),
    gender: Joi.string().label('Gender'),
  }

  submitForm = () => {
    this.setState({ submitted: true })
    console.log('Submitted')
  }

  render() {
    const customStyles = {
      menu: (provided) => ({
        ...provided,
        width: '100%',
        color: 'black',
        padding: 10,
        backgroundColor: '#79727c',
      }),
    }

    const options = [
      { value: 'JavaScript', label: 'JavaScript' },
      { value: 'Java', label: 'Java' },
      { value: 'Python', label: 'Python' },
    ]
    const radioButton = [
      { label: 'Male', value: 'male' },
      { label: 'Female', value: 'female' },
    ]

    return (
      <div className='form-background'>
        <i className='fas fa-code'></i>
        <h1>Join over 25 million learners from around the globe</h1>
        <p>
          Master the languages of the web: HTML,CSS,and JavaScript. This path
          will prepare you to build basic websites and then build interactive
          web apps
        </p>
        <form onSubmit={this.handleSubmit}>
          <Input
            name='name'
            type='text'
            label='Full Name'
            value={this.state.data.name}
            onChange={this.handleChange}
            error={this.state.errors.name}
          />
          <Input
            name='username'
            type='text'
            label='Username'
            value={this.state.data.username}
            onChange={this.handleChange}
            error={this.state.errors.username}
          />
          <Input
            name='email'
            type='email'
            label='Email'
            value={this.state.data.email}
            onChange={this.handleChange}
            error={this.state.errors.email}
          />
          <Input
            name='password'
            type='password'
            label='Password'
            value={this.state.data.password}
            onChange={this.handleChange}
            error={this.state.errors.password}
          />
          <Radio
            radioButton={radioButton}
            name='gender'
            type='radio'
            onChange={this.handleChange}
            error={this.state.errors.gender}
          />
          <Input
            name='phoneNumber'
            type='number'
            label='Phone Number'
            value={this.state.data.phoneNumber}
            onChange={this.handleChange}
            error={this.state.errors.phoneNumber}
          />
          <Input
            name='address'
            type='textarea'
            label='Address'
            value={this.state.data.address}
            onChange={this.handleChange}
            error={this.state.errors.address}
          />

          <Select
            name='selectedLanguage'
            styles={customStyles}
            placeholder='Select Language'
            value={this.state.data.selectedLanguage}
            onChange={this.selectHandler}
            options={options}
          />

          {this.state.errors.selectedLanguage &&
          !this.state.data.selectedLanguage ? (
            <small className='error'>Select an Language</small>
          ) : (
            <small className='error-none'>{''}</small>
          )}
          {this.state.submitted ? (
            <div className='submitted'>Done</div>
          ) : (
            <React.Fragment>
              <button className='btn' type='submit'>
                Submit
              </button>
              <FormFooter />
            </React.Fragment>
          )}
        </form>
      </div>
    )
  }
}

export default SignUpForm
