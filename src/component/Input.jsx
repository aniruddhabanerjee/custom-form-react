import React from 'react'

const Input = ({ name, value, onChange, type, label, error }) => {
  return (
    <div className='form-group'>
      {type === 'textarea' && (
        <div>
          <textarea
            className='input-box'
            name={name}
            id={name}
            value={value}
            placeholder={`Enter your ${label}`}
            onChange={onChange}
          />
          {error ? (
            <small className='error'>{error}</small>
          ) : (
            <small className='error-none'>{''}</small>
          )}
        </div>
      )}
      {['text', 'number', 'password', 'email'].includes(type) && (
        <React.Fragment>
          <input
            className='input-box'
            name={name}
            id={name}
            type={type}
            value={value}
            placeholder={`Enter your ${label}`}
            onChange={onChange}
          />
          {error ? (
            <small className='error'>{error}</small>
          ) : (
            <small className='error-none'>{''}</small>
          )}
        </React.Fragment>
      )}
    </div>
  )
}

export default Input

// import React, { Component } from 'react'

// export class Input extends Component {
//   state = {}
//   handleChange = (e) => {
//     const { name, value } = e.target
//     this.setState({ [name]: value })
//     this.props.setInputValue(name.toLowerCase(), value)
//   }
//   render() {
//     const formElement = this.props.inputField.map((val) => {
//       return (
//         <div className='form-group' key={val.name}>
//           <input
//             className='input-box'
//             name={val.name}
//             type={val.type}
//             value={this.state[val.name] || ''}
//             placeholder={`Enter your ${val.name}`}
//             onChange={this.handleChange}
//           />
//         </div>
//       )
//     })
//     return <div>{formElement}</div>
//   }
// }

// export default Input
